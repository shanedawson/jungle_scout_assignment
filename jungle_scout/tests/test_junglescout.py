import logging

import pytest

from jungle_scout.const import ERR_ASIN_INVALID, ERR_ASIN_LENGTH


@pytest.fixture
def client(app):
    app.config['TESTING'] = True
    client = app.test_client()
    yield client


def test_index(client):
    rv = client.get('/')
    assert b'js-container-root' in rv.data


asins = [
    'B075WW3JKQ',
    'B01M29RMSZ',
    'B079BS4ZQ4',
    'B07HKMXJLY',
    '1234567890'
]


def test_lookup(client):
    for asin in asins:
        logging.warning(f'Testing ASIN: {asin}')
        rv = client.get(f'/api/lookup/{asin}')
        data = rv.get_json()
        assert data['asin'] == asin
        # assert data['mass']
        # assert data['height']
        # assert data['width']
        assert data['category']
        assert data['name']
        assert data['rank']


def test_invalid_asin(client):
    bad_asin = 'ABCD123456'
    rv = client.get(f'/api/lookup/{bad_asin}')
    data = rv.get_json()
    assert data['error'] == ERR_ASIN_INVALID

def test_incorrect_length_asin(client):
    bad_asin = 'ABCD12345'
    rv = client.get(f'/api/lookup/{bad_asin}')
    data = rv.get_json()
    assert data['error'] == ERR_ASIN_LENGTH
