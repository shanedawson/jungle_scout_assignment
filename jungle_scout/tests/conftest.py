import pytest
from jungle_scout.app import create_app


@pytest.fixture
def app():
    app = create_app()
    return app


