from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class ProductDetails(db.Model):
    __table_name__ = 'product_details'

    asin = db.Column(db.Text(), primary_key=True)
    name = db.Column(db.Text())
    category = db.Column(db.Text())
    rank = db.Column(db.Text())
    width = db.Column(db.Float())
    height = db.Column(db.Float())
    depth = db.Column(db.Float())
    mass = db.Column(db.Integer())
