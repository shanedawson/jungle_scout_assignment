# Installation
* Setup a Python 3.6 virtual_env
* Install requirements.txt with pip or pipenv
```
    pip install -r requirements.txt
```

* Install nodejs modules with yarn
```
    cd jungle_scout/static
    yarn install
```

* Create an sqlite3 database
```
    cd jungle_scout
    sqlite3 app.db
    .quit
```

* Run migrations
```
    cd jungle_scout
    flask db upgrade
```


## Running
* Run webpack dev server
```
    cd jungle_scout/static
    yarn run start
```

* Run Flask
```
    cd jungle_scout
    flask run
```

* Application will be running at localhost:5000


