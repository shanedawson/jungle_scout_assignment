import logging
import re
from typing import Dict, Optional

import requests
from lxml import html
from lxml.html import HtmlElement
from sqlalchemy import inspect

session = requests.session()
session.headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'
}

XPATH_NAME = '//span[@id="productTitle"]//text()'
XPATH_CATEGORY = '//a[@class="a-link-normal a-color-tertiary"]//text()'
XPATH_RANK = '//*[@id="SalesRank"]//text()'
# Dimensions and weight don't always appear under the same tag
XPATH_DIMENSIONS = '//*[contains(text(),"Product Dimensions")]'
XPATH_MASS = '//*[contains(text(),"Item Weight")]'


def extract_details(content):
    """Extract the product details from the html string"""
    doc = html.fromstring(content)
    raw_name = doc.xpath(XPATH_NAME)
    raw_category = doc.xpath(XPATH_CATEGORY)
    raw_dimensions = doc.xpath(XPATH_DIMENSIONS)
    raw_mass = doc.xpath(XPATH_MASS)
    raw_rank = doc.xpath(XPATH_RANK)

    name = ''.join(raw_name).strip()
    category = ' > '.join([cat.strip() for cat in raw_category])

    data = {
        'name': name,
        'category': category,
    }
    if raw_rank:
        rank_text = list(filter(lambda r: str.isprintable(r[0]) and 'top 100' not in r.lower(), raw_rank))
        rank = ' '.join(entry.strip() for entry in rank_text[1:])
        data['rank'] = rank

    if raw_dimensions:
        data.update(**extract_dimensions(raw_dimensions[0]))
    mass = None
    if raw_mass:
        mass = extract_mass(raw_mass[0])
    elif raw_dimensions:
        mass = extract_mass(raw_dimensions[0])
    if mass:
        data['mass'] = mass
    logging.info(data)
    return data


def extract_text_from_tag(el: HtmlElement) -> str:
    text = None
    if el.tag == 'b':
        text = el.tail.strip()
    elif el.tag == 'td':
        text = el.getnext().text.strip()
    return text


def extract_dimensions(raw_dimensions: HtmlElement) -> Dict[str, float]:
    dimensions = extract_text_from_tag(raw_dimensions)
    size = dimensions.split(' cm')[0]
    width, height, depth = [float(dim.strip()) for dim in size.split('x')]
    return {
        'width': width,
        'height': height,
        'depth': depth
    }


def extract_mass(el: HtmlElement) -> Optional[float]:
    mass = None
    try:
        search_str = extract_text_from_tag(el)
        if ';' in search_str:
            search_str = search_str.split('; ')[1]
    except AttributeError:
        return None
    else:
        res = re.search(r'(.*)\s(K?g)', search_str)
        if res:
            mass = res.group(1)
            unit = res.group(2)
            if mass and unit:
                mass = float(mass)
                if unit.lower() == 'kg':
                    mass = mass * 1000

    return mass


def object_as_dict(obj) -> Dict:
    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}
