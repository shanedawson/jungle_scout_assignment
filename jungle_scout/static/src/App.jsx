import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import { Grid, Row, Col } from 'react-bootstrap';
import SearchBox from './components/SearchBox';
import ResultTable from './components/ResultTable';
import { Header } from './components/Header';
import Error from './components/Error';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <Header />
        <Grid>
          <Row className="search-box">
            <Col lg={12}>
              <SearchBox />
              <Error />
            </Col>
          </Row>
          <Row>
            <ResultTable />
          </Row>
        </Grid>
      </>
    );
  }
}

export default hot(module)(App);
