import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './store.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App.jsx';

const rootSelector = document.getElementById('js-container-root');

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootSelector,
);
