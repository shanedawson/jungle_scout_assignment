import * as actions from '../actions';

const initialState = {
  error: null,
  data: [],
};

const reducer = (state = initialState, action) => {
  const handlers = {
    [actions.LOOKUP_END]() {
      if (action.data.hasOwnProperty('error')) {
        return Object.assign({}, state, { error: action.data.error });
      }

      return Object.assign({}, state, {
        data: [action.data, ...state.data],
        error: null,
      });
    },
    [actions.FETCH_ITEMS_END]() {
      return Object.assign({}, state, { data: action.data });
    },
  };

  if (handlers.hasOwnProperty(action.type)) {
    return handlers[action.type](state, action);
  }
  return state;
};

export default reducer;
