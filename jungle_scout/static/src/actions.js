export const LOOKUP = 'LOOKUP';
export const LOOKUP_END = 'LOOKUP_END';
export const LOOKUP_FAIL = 'LOOKUP_FAIL';
export const FETCH_ITEMS = 'FETCH_ITEMS';
export const FETCH_ITEMS_FAIL = 'FETCH_ITEMS_FAIL';
export const FETCH_ITEMS_END = 'FETCH_ITEMS_END';

export const fetchJson = async (endpoint, method = 'GET', body = null) => {
  const url = `${endpoint}`;
  try {
    const resp = await fetch(url, {
      credentials: 'same-origin',
      method,
      ...(method === 'POST' || 'PUT' ? { body } : {}),
    });
    return resp.json();
  } catch (e) {
    await Promise.reject(e);
  }
};

export const lookupItem = asin => async dispatch => {
  dispatch({ type: LOOKUP, asin });
  const endpoint = `/api/lookup/${asin}`;
  try {
    const json = await fetchJson(endpoint);
    await dispatch({ type: LOOKUP_END, data: json });
  } catch (e) {
    await dispatch({ type: LOOKUP_FAIL, error: e });
  }
};

export const fetchItems = () => async dispatch => {
  dispatch({ type: FETCH_ITEMS });
  const endpoint = `/api/list/`;
  try {
    const json = await fetchJson(endpoint);
    await dispatch({ type: FETCH_ITEMS_END, data: json });
  } catch (e) {
    await dispatch({ type: FETCH_ITEMS_FAIL, error: e });
  }
};
