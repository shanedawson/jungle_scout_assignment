import React, { Component } from 'react';
import { connect } from 'react-redux';
import BootstrapTable from 'react-bootstrap-table-next';
import PropTypes from 'prop-types';
import { fetchItems } from '../actions';

const columns = [
  {
    dataField: 'asin',
    text: 'ASIN',
    sort: true,
  },
  {
    dataField: 'name',
    text: 'Name',
    sort: true,
  },
  {
    dataField: 'category',
    text: 'Category',
  },
  {
    dataField: 'rank',
    text: 'Rank',
  },
  {
    dataField: 'width',
    text: 'Width(cm)',
    sort: true,
  },
  {
    dataField: 'height',
    text: 'Height(cm)',
    sort: true,
  },
  {
    dataField: 'depth',
    text: 'Depth(cm)',
    sort: true,
  },
  {
    dataField: 'mass',
    text: 'Mass(g)',
    sort: true,
  },
];

class ResultTable extends Component {
  componentDidMount() {
    this.props.fetchItems();
  }

  render() {
    const { data } = this.props;
    return (
      <BootstrapTable
        striped
        hover
        keyField="asin"
        columns={columns}
        data={data}
      />
    );
  }
}

const mapStateToProps = state => ({
  data: state.reducer.data,
});

const mapDispatchToProps = dispatch => ({
  fetchItems: args => dispatch(fetchItems(args)),
});

ResultTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  fetchItems: PropTypes.func,
};

export default (ResultTable = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResultTable));
