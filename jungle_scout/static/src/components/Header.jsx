import React from 'react';
import { Navbar } from 'react-bootstrap';

export const Header = () => (
  <Navbar>
    <Navbar.Brand>
      <a href="/">ASIN Lookup</a>
    </Navbar.Brand>
  </Navbar>
);
