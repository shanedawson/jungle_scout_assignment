import React, { Component } from 'react';
import { bindActionCreators, connect } from 'react-redux';
import {
  Button,
  FormGroup,
  FormControl,
  InputGroup,
  Glyphicon,
} from 'react-bootstrap';
import { lookupItem } from '../actions';

class SearchBox extends Component {
  state = {
    query: '',
  };

  handleInputChange = e => {
    e.preventDefault();
    this.setState({
      query: e.target.value,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.lookupItem(this.state.query);
  };

  getValidationState = () => {
    if (!this.state.query) {
      return null;
    }
    if (this.state.query.length === 10) {
      return 'success';
    }

    return 'error';
  };

  render() {
    return (
      <FormGroup bsSize="large" validationState={this.getValidationState()}>
        <InputGroup bsSize="large">
          <FormControl
            type="text"
            placeholder="ASIN"
            onChange={this.handleInputChange}
          />
          <InputGroup.Button>
            <Button aria-label="Search" onClick={this.handleSubmit}>
              <Glyphicon glyph="search" />
            </Button>
          </InputGroup.Button>
        </InputGroup>
      </FormGroup>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  lookupItem: args => dispatch(lookupItem(args)),
});

export default (SearchBox = connect(
  null,
  mapDispatchToProps,
)(SearchBox));
