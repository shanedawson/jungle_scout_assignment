import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';

const _Error = ({ msg }) => msg && <Alert bsStyle="danger">{msg}</Alert>;

const mapStateToProps = state => ({
  msg: state.reducer.error,
});

_Error.propTypes = {
  msg: PropTypes.object,
};

const Error = connect(mapStateToProps)(_Error);
export default Error;
