from flask import Flask, render_template
from flask_migrate import Migrate


def create_app():
    app = Flask(__name__, static_folder='static', template_folder='templates')
    app.config.from_pyfile('config.py')
    from jungle_scout.models import db
    db.init_app(app)
    migrate = Migrate(app, db)
    from jungle_scout.views import api
    app.register_blueprint(api, url_prefix='/api')

    @app.route('/')
    def index_view():
        return render_template('index.html')

    return app
