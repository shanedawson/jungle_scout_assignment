from flask import jsonify, Blueprint

from jungle_scout.const import ERR_ASIN_LENGTH, ERR_ASIN_INVALID
from jungle_scout.models import db, ProductDetails
from jungle_scout.utils import session, extract_details, object_as_dict

api = Blueprint('api', __name__)


@api.route('list/', methods=['GET'])
def list_products():
    items = db.session.query(ProductDetails).all()
    data = [object_as_dict(item) for item in items]
    return jsonify(data)


@api.route('/lookup/<asin>')
def lookup(asin):
    if len(asin) != 10:
        return json_error_response(status_code=400, reason=ERR_ASIN_LENGTH)
    url = f'https://www.amazon.ca/dp/{asin}/'
    resp = session.get(url)
    if resp.status_code != 200:
        if resp.status_code == 404:
            return json_error_response(status_code=200, reason=ERR_ASIN_INVALID)
        else:
            return json_error_response(**resp.__dict__)
    data = dict(asin=asin, **extract_details(resp.content))
    obj = ProductDetails(**data)
    db.session.merge(obj)
    db.session.commit()

    return jsonify(data)


def json_error_response(status_code, reason, **kwargs):
    return jsonify({
        'status': status_code,
        'error': reason
    })
